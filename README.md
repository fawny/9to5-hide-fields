# hide-fields

Hides a profile on Contribution pages. The ID of the profile is hard-coded.