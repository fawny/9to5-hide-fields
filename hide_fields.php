<?php

require_once 'hide_fields.civix.php';
use CRM_HideFields_ExtensionUtil as E;

/**
 * Implements hook_civicrm_buildForm() to add CSS to certain forms which hides certain profiles.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 *
 */
function hide_fields_civicrm_buildForm($formName, &$form) {
  if (!in_array($form->getVar('_id'), [8, 12, 20, 25, 31])) {
    return;
    //'Year-End Campaign 2019' => 8
    //'Year-End Campaign 2020' => 12
    //'2021_NATL_Year End Team Campaign umbrella' => 20
    //'2022_NATL_Year End Team Campaign umbrella' => 25
    //'2023_NATL_Year End Team Campaign umbrella' => 31
  }
  switch ($formName) {
    case 'CRM_Contribute_Form_Contribution_Main':
      CRM_Core_Resources::singleton()->addStyle('.crm-profile-name-Year_End_Campaign_Contribution_Source_33 { display: none; }');
      break;
    
    case 'CRM_Contribute_Form_Contribution_Confirm':
    case 'CRM_Contribute_Form_Contribution_ThankYou':
      CRM_Core_Resources::singleton()->addStyle('.crm-profile-view:last-of-type { display: none; }');
      break;
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function hide_fields_civicrm_config(&$config) {
  _hide_fields_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function hide_fields_civicrm_install() {
  _hide_fields_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function hide_fields_civicrm_enable() {
  _hide_fields_civix_civicrm_enable();
}
